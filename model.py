import pygame 
from random import randint
from options import *

class Enemy:
    def __init__(self, x, layout, HP, speed=1, color=(130,30,30), radius=3):
        self.x = x
        self.y = randint(0,size[1]-10)
        self.layout = layout
        self.HP = HP
        self.speed = speed
        self.color = color
        self.radius = radius
    def run(self):
        pass
        

class ball:
    def __init__(self, x, y, layout, speed=3, color=(50,140,30), radius=3):
        self.x = x
        self.y = y
        self.layout = layout
        self.color = color
        self.radius = radius
        self.speed = speed
    def run(self):
        pygame.draw.circle(self.layout, self.color, (self.x, self.y), self.radius)
        self.x += self.speed
        if self.x > size[0] or self.x < 0:
            del self


class CharacterController():
    KD = 10
    ball_list = []
    def __init__(self, x, y, layout, speed=4, color=(30,30,140)):
        self.x = x
        self.y = y
        self.layout = layout
        self.speed = speed
        self.color = color
    def move(self):
        pygame.draw.rect(self.layout, self.color, (self.x, self.y, 20, 20))
        keys = pygame.key.get_pressed()
        if keys[pygame.K_LEFT] and keys[pygame.K_DOWN] and self.x > 0 and self.y < size[1]-20:
            self.x -= self.speed-1
            self.y += self.speed-1
        elif keys[pygame.K_LEFT] and keys[pygame.K_UP] and self.x > 0 and self.y > 0:
            self.x -= self.speed-1
            self.y -= self.speed-1
        elif keys[pygame.K_RIGHT] and keys[pygame.K_UP] and self.x < size[0]-20 and self.y > 0:
            self.x += self.speed-1
            self.y -= self.speed-1
        elif keys[pygame.K_RIGHT] and keys[pygame.K_DOWN] and self.x < size[0]-20 and self.y < size[1]-20:
            self.x += self.speed-1
            self.y += self.speed-1
        elif keys[pygame.K_LEFT] and self.x > 0:
            self.x -= self.speed
        elif keys[pygame.K_RIGHT] and self.x < size[0]-20:
            self.x += self.speed
        elif keys[pygame.K_DOWN] and self.y < size[1]-20:
            self.y += self.speed
        elif keys[pygame.K_UP] and self.y > 0:
            self.y -= self.speed
        if keys[pygame.K_e] and self.KD < 0:
            self.ball_list.append(ball(self.x+10, self.y+10, self.layout))            
            print("poof")
            self.KD = 10
        else:
            self.KD -= 1
