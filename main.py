import pygame
from options import *
from model import *

pygame.init()
layout = pygame.display.set_mode(size)
pygame.display.set_caption('test game')
clock = pygame.time.Clock()
run = True
player = CharacterController(100, 100, layout, color=(255,255,0))


if __name__ == "__main__":
	while run:
		clock.tick(100)
		layout.fill(BG)
		player.move()
		for e in player.ball_list:
			e.run()
		for event in pygame.event.get():
			if event.type == pygame.QUIT:
				run = False
		
		pygame.display.update()
		pygame.display.flip()
	pygame.quit()